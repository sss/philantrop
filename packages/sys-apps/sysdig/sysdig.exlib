# Copyright 2014-2017 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=draios ] cmake [ api=2 ] bash-completion zsh-completion

export_exlib_phases src_prepare src_install

SUMMARY="system-level exploration and troubleshooting tool"
DESCRIPTION="
Sysdig captures system calls and other system level events using a linux kernel
facility called tracepoints, which means much less overhead than strace.
It then 'packetizes' this information, so that you can save it into trace files
and filter it, a bit like you would do with tcpdump. This makes it very flexible
to explore what processes are doing.
"
HOMEPAGE="https://www.${PN}.org"

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/lua[=5.1*]
        dev-libs/jsoncpp:*
    build+run:
        app-text/jq[>=1.5]
        dev-libs/libb64
        net-misc/curl
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSYSDIG_VERSION:STRING="${PV}"
    -DBUILD_DRIVER:BOOL=off
    -DDIR_ETC=/etc
    -DUSE_BUNDLED_DEPS:BOOL=off
    -DUSE_BUNDLED_CURL:BOOL=off
    -DUSE_BUNDLED_JQ:BOOL=off
    -DUSE_BUNDLED_JSONCPP:BOOL=off
    -DUSE_BUNDLED_LUAJIT:BOOL=off
    -DUSE_BUNDLED_NCURSES:BOOL=off
    -DUSE_BUNDLED_OPENSSL:BOOL=off
    -DUSE_BUNDLED_ZLIB:BOOL=off
    -DUSE_BUNDLED_B64:BOOL=off
)

DEFAULT_SRC_COMPILE_PARAMS=( V=1 )

sysdig_src_prepare() {
    default

    edo pushd "${CMAKE_SOURCE}"

    edo sed \
        -e "/DESTINATION/s:\(share\)\(.*\):/usr/\1\2:" \
        -i scripts/CMakeLists.txt \
        -i userspace/sysdig/CMakeLists.txt \
        -i userspace/sysdig/man/CMakeLists.txt

    edo sed \
        -e '/DESTINATION/s:\(src\)/\(${PACKAGE_NAME}-${PROBE_VERSION}\)\(.*\):/usr/\1/sysdig\":' \
        -i driver/CMakeLists.txt

    edo popd
}

sysdig_src_install() {
    cmake_src_install

    edo rm -r "${IMAGE}"/etc/{bash_completion.d,}
    edo rm -r "${IMAGE}"/usr/share/zsh/{site-functions,}

    option bash-completion && dobashcompletion "${CMAKE_SOURCE}"/scripts/completions/bash/sysdig
    option zsh-completion && dozshcompletion "${CMAKE_SOURCE}"/scripts/completions/zsh/_sysdig
}

